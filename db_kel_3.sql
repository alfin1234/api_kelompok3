-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jun 2023 pada 17.15
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kel_3`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kodeBarang` char(12) NOT NULL,
  `namaBarang` varchar(100) NOT NULL,
  `jumlahBarang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kodeBarang`, `namaBarang`, `jumlahBarang`) VALUES
('B01', 'Komputer', 50),
('B02', 'Laptop', 100),
('B03', 'Printer', 111),
('B04', 'Keyboard', 200);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan`
--

CREATE TABLE `pengajuan` (
  `kodePengajuan` char(12) NOT NULL,
  `tglPengajuan` date NOT NULL,
  `npmPeminjam` char(12) NOT NULL,
  `namaPeminjam` varchar(100) NOT NULL,
  `mhsProdi` varchar(100) NOT NULL,
  `noHandphone` char(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengajuan`
--

INSERT INTO `pengajuan` (`kodePengajuan`, `tglPengajuan`, `npmPeminjam`, `namaPeminjam`, `mhsProdi`, `noHandphone`) VALUES
('PJ01', '2023-06-01', '19311021', 'Chairul Fahri', 'Sistem Informasi', '082280808080'),
('PJ02', '2023-01-01', '19311024', 'Wisnu Mukti', 'sistem informasi', '089929299');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuandetail`
--

CREATE TABLE `pengajuandetail` (
  `kodePengajuan` char(12) NOT NULL,
  `kodeBarang` char(12) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengajuandetail`
--

INSERT INTO `pengajuandetail` (`kodePengajuan`, `kodeBarang`, `jumlah`) VALUES
('PJ01', 'B01', 50);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembalian`
--

CREATE TABLE `pengembalian` (
  `kodePengembalian` char(12) NOT NULL,
  `kodePengajuan` char(12) NOT NULL,
  `tglKembali` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengembalian`
--

INSERT INTO `pengembalian` (`kodePengembalian`, `kodePengajuan`, `tglKembali`) VALUES
('PB01', 'PJ01', '2023-06-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengembaliandetail`
--

CREATE TABLE `pengembaliandetail` (
  `kodePengembalian` char(12) NOT NULL,
  `kodeBarang` char(12) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengembaliandetail`
--

INSERT INTO `pengembaliandetail` (`kodePengembalian`, `kodeBarang`, `jumlah`) VALUES
('PB01', 'B01', 50);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `name`, `username`, `pass`, `role`) VALUES
(1, 'admin', 'tes', '28b662d883b6d76fd96e4ddc5e9ba780', 'Admin'),
(2, 'admin2', 'Admin2', '7a8a80e50f6ff558f552079cefe2715d', 'Admin'),
(4, 'tess1', 'tes11', '22daf1a39b6e5ea16554f59e472d96f6', 'Admin'),
(6, 'admin1', 'admin1', '0192023a7bbd73250516f069df18b500', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kodeBarang`);

--
-- Indeks untuk tabel `pengajuan`
--
ALTER TABLE `pengajuan`
  ADD PRIMARY KEY (`kodePengajuan`);

--
-- Indeks untuk tabel `pengajuandetail`
--
ALTER TABLE `pengajuandetail`
  ADD PRIMARY KEY (`kodePengajuan`);

--
-- Indeks untuk tabel `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`kodePengembalian`);

--
-- Indeks untuk tabel `pengembaliandetail`
--
ALTER TABLE `pengembaliandetail`
  ADD PRIMARY KEY (`kodePengembalian`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
